import threading
import operator
import random
import time
import sys
import numpy as np

from resource.ColorPackage import RED_COLOR, BLUE_COLOR, GREEN_COLOR, YELLOW_COLOR, EMPTY
from resource.GeneticsResource import MUTATION_CHANCE, MUTATION_INTENSITY, NEXT_GENERATION_SIZE, ACTIVE_COLOR, \
    PARENTS_IN_FAMILY, NUMBER_OF_FAMILIES, CHILDREN_IN_FAMILY
from resource.ColorType import RED_TYPE, GREEN_TYPE, BLUE_TYPE, YELLOW_TYPE, BUTTON_TYPE
from resource.ButtonType import CIRCLE, PLUS, DIAGONAL


class GeneticsHelper(threading.Thread):
    def __init__(self, **kwargs):
        super(GeneticsHelper, self).__init__()
        self.number_of_breads = 5

        self.isRunning = False
        self.isSolving = False

        self.gameInfoBox = None
        self.gameScreen = None
        self.gameScreenHelper = None

        self.default_color = None
        self.settings = {
            NEXT_GENERATION_SIZE: None,
            PARENTS_IN_FAMILY: None,
            CHILDREN_IN_FAMILY: None,
            NUMBER_OF_FAMILIES: None,
            MUTATION_CHANCE: None,
            MUTATION_INTENSITY: None,
            ACTIVE_COLOR: None
        }

        self.button_index_list = None
        self.button_type_list = None

        self.genome_size = None

        self.original_grid = None

        self.current_generation_list = None

    def run(self):
        self.isRunning = True
        self.pre_setup()
        while self.isRunning:
            self.genetics()

    def resume(self):
        self.isSolving = True

    def stop(self):
        self.isSolving = False

    def set_running_settings(self):
        self.settings[MUTATION_CHANCE] = self.gameInfoBox.get_mutation_chance()
        self.settings[MUTATION_INTENSITY] = self.gameInfoBox.get_mutation_intensity()
        self.settings[NEXT_GENERATION_SIZE] = self.gameInfoBox.get_generation_size()
        self.settings[PARENTS_IN_FAMILY] = self.gameInfoBox.get_parents_in_family()
        self.settings[NUMBER_OF_FAMILIES] = self.gameInfoBox.get_number_of_families()
        self.settings[CHILDREN_IN_FAMILY] = self.gameInfoBox.get_children_in_family()

        active_color = self.gameInfoBox.get_active_color()
        self.settings[ACTIVE_COLOR] = active_color if active_color != BUTTON_TYPE else self.default_color

    def generate_random_gene(self):
        return random.randint(0, 3)

    def pre_setup(self):
        self.current_generation_list = []
        for counter in range(10):
            genome = []
            for i in range(self.genome_size):
                genome.append(self.generate_random_gene())
            self.current_generation_list.append(genome)

    def select_parents(self):
        parent_list = []
        while len(parent_list) < self.settings[PARENTS_IN_FAMILY]:
            parent = random.randint(0, len(self.current_generation_list) - 1)
            if parent not in parent_list:
                parent_list.append(parent)
        return parent_list

    def generate_child(self, parent_list):
        child = [None] * self.genome_size
        gene_index_poll = range(0, self.genome_size)
        random.shuffle(gene_index_poll)
        can_mutate = self.can_mutation()
        while len(gene_index_poll) != 0:
            random_parent = random.randint(0, len(parent_list) - 1)
            random_gene_index = gene_index_poll.pop()
            random_gene = None if not can_mutate else self.mutate_gene()
            if random_gene is None:
                child[random_gene_index] = self.current_generation_list[parent_list[random_parent]][random_gene_index]
            else:
                child[random_gene_index] = random_gene
        return child

    def get_child_grid(self, child):
        child_grid = np.array(self.original_grid)
        for gene_index in range(self.genome_size):
            widget_list = self.gameScreenHelper.button_action(button_type=self.button_type_list[gene_index],
                                                              button_row=self.button_index_list[gene_index][0],
                                                              button_column=self.button_index_list[gene_index][1],
                                                              update_color=False)
            for position in widget_list:
                for i in range(child[gene_index]):
                    self.gameScreenHelper.set_next_color(position=position, grid=child_grid)
        return child_grid

    def get_fitness(self, grid):
        fitness = 0
        for row in grid:
            for value in row:
                if value == self.settings[ACTIVE_COLOR]:
                    fitness += 1
        return fitness

    def set_new_generation(self, child_map, fitness_map):
        sorted_fitness_map = sorted(fitness_map.items(), key=operator.itemgetter(1))
        next_generation_size = self.settings[NEXT_GENERATION_SIZE]
        top_child = None
        self.current_generation_list = []
        for fitness_tuple in sorted_fitness_map[::-1]:
            if top_child is None:
                top_child = fitness_tuple[0]
            if next_generation_size > 0:
                self.current_generation_list.append(child_map[fitness_tuple[0]])
                next_generation_size -= 1
        return top_child

    def can_mutation(self):
        mutation_chance = random.uniform(0.0, 100.0)
        if mutation_chance <= self.settings[MUTATION_CHANCE]:
            return True
        else:
            return False

    def mutate_gene(self):
        mutation_chance = random.uniform(0.0, 100.0)
        if mutation_chance <= self.settings[MUTATION_INTENSITY]:
            return self.generate_random_gene()
        else:
            return None

    def genetics(self):
        while self.isSolving:
            self.set_running_settings()
            child_map = {}
            fitness_map = {}
            grid_map = {}
            child_number = 0
            for family in range(self.settings[NUMBER_OF_FAMILIES]):
                parent_list = self.select_parents()
                for child_in_family in range(self.settings[CHILDREN_IN_FAMILY]):
                    child = self.generate_child(parent_list=parent_list)
                    grid = self.get_child_grid(child=child)
                    fitness = self.get_fitness(grid=grid)
                    child_map[child_number] = child
                    fitness_map[child_number] = fitness
                    grid_map[child_number] = grid
                    child_number += 1
            top_child = self.set_new_generation(child_map=child_map, fitness_map=fitness_map)
            self.gameScreen.update_grid(grid_map[top_child])
            time.sleep(0.5)
