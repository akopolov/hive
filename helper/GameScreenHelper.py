import random
import numpy as np
from handler.CustomWidget import CustomWidget
from handler.CustomButton import CustomButton
from resource.ColorPackage import RED_COLOR, BLUE_COLOR, GREEN_COLOR, YELLOW_COLOR, EMPTY
from resource.ColorType import RED_TYPE, GREEN_TYPE, BLUE_TYPE, YELLOW_TYPE, BUTTON_TYPE
from resource.ButtonType import CIRCLE, PLUS, DIAGONAL


class GameScreenHelper(object):
    def __init__(self, element_array, x, y):
        self.element_array = element_array
        self.button_type_list = None
        self.length_x = x - 1
        self.length_y = y - 1

    def get_color_by_type(self, color_type):
        if color_type == RED_TYPE:
            return RED_COLOR

        if color_type == GREEN_TYPE:
            return GREEN_COLOR

        if color_type == BLUE_TYPE:
            return BLUE_COLOR

        if color_type == YELLOW_TYPE:
            return YELLOW_COLOR

    def get_color_by_position(self, position):
        return self.get_color_by_type(color_type=self.element_array[position[0]][position[1]])

    def get_next_color_type_by_type(self, current_color_type):
        if current_color_type == RED_TYPE:
            return GREEN_TYPE

        if current_color_type == GREEN_TYPE:
            return BLUE_TYPE

        if current_color_type == BLUE_TYPE:
            return YELLOW_TYPE

        if current_color_type == YELLOW_TYPE:
            return RED_TYPE

    def set_next_color(self, position, grid=None):
        if grid is None:
            grid = self.element_array
        current_color_type = grid[position[0]][position[1]]
        grid[position[0]][position[1]] = self.get_next_color_type_by_type(current_color_type=current_color_type)

    def create_button_map(self, x, y, number_of_buttons):
        self.button_type_list = []
        return list(zip(
            np.random.randint(x, size=number_of_buttons),
            np.random.randint(y, size=number_of_buttons)
        ))

    def create_button(self, row, column):
        button = CustomButton()
        button.row = row
        button.column = column
        button.type = random.choice([CIRCLE, PLUS, DIAGONAL])
        button.background_normal = 'resource/' + str(button.type) + '.png'
        self.element_array[row][column] = BUTTON_TYPE
        self.button_type_list.append(button.type)
        return button

    def create_widget(self, row, column, color_type):
        widget = CustomWidget()
        widget.row = row
        widget.column = column
        widget.color_type = color_type
        widget.rgb = self.get_color_by_type(color_type)
        self.element_array[row][column] = color_type
        return widget

    def button_circle(self, row, column, update_color):
        widget_array = []
        for position in [(row, column - 1), (row, column + 1), (row - 1, column - 1), (row - 1, column),
                         (row - 1, column + 1), (row + 1, column - 1), (row + 1, column), (row + 1, column + 1)]:
            if not (position[0] < 0 or position[1] < 0 or position[0] > self.length_x or
                    position[1] > self.length_y or self.element_array[position[0]][position[1]] == BUTTON_TYPE or
                    (row == position[0] and column == position[1])):
                widget_array.append(position)
                if update_color:
                    self.set_next_color(position=position)
        return widget_array

    def button_diagonal(self, row, column, update_color):
        widget_array = []
        row_add = 0
        column_add = 0
        not_completed = [True, True, True, True]
        while not_completed[0] or not_completed[1] or not_completed[2] or not_completed[3]:
            row_add += 1
            column_add += 1
            if not_completed[0] and row + row_add <= self.length_x and column + column_add <= self.length_y:
                position = (row + row_add, column + column_add)
                if not (self.element_array[position[0]][position[1]] == BUTTON_TYPE):
                    widget_array.append(position)
                    if update_color:
                        self.set_next_color(position=position)
            else:
                not_completed[0] = False

            if not_completed[1] and row + row_add <= self.length_x and column - column_add >= 0:
                position = (row + row_add, column - column_add)
                if not (self.element_array[position[0]][position[1]] == BUTTON_TYPE):
                    widget_array.append(position)
                    if update_color:
                        self.set_next_color(position=position)
            else:
                not_completed[1] = False

            if not_completed[2] and row - row_add >= 0 and column + column_add <= self.length_y:
                position = (row - row_add, column + column_add)
                if not (self.element_array[position[0]][position[1]] == BUTTON_TYPE):
                    widget_array.append(position)
                    if update_color:
                        self.set_next_color(position=position)
            else:
                not_completed[2] = False

            if not_completed[3] and row - row_add >= 0 and column - column_add >= 0:
                position = (row - row_add, column - column_add)
                if not (self.element_array[position[0]][position[1]] == BUTTON_TYPE):
                    widget_array.append(position)
                    if update_color:
                        self.set_next_color(position=position)
            else:
                not_completed[3] = False
        return widget_array

    def button_plus(self, row, column, update_color):
        widget_array = []
        for x in range(self.length_x + 1):
            if not (self.element_array[x][column] == BUTTON_TYPE or row == x):
                position = (x, column)
                widget_array.append(position)
                if update_color:
                    self.set_next_color(position=position)
        for y in range(self.length_y + 1):
            if not (self.element_array[row][y] == BUTTON_TYPE or column == y):
                position = (row, y)
                widget_array.append(position)
                if update_color:
                    self.set_next_color(position=position)
        return widget_array

    def button_action(self, button_type, button_row, button_column, update_color=True):
        if button_type == CIRCLE:
            return self.button_circle(row=button_row, column=button_column, update_color=update_color)

        if button_type == DIAGONAL:
            return self.button_diagonal(row=button_row, column=button_column, update_color=update_color)

        if button_type == PLUS:
            return self.button_plus(row=button_row, column=button_column, update_color=update_color)
