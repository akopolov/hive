import random
import numpy as np
from kivy.lang import Builder
from kivy.uix.screenmanager import Screen

from kivy.properties import ObjectProperty
from kivy.clock import Clock

from resource.ColorPackage import RED_COLOR, BLUE_COLOR, GREEN_COLOR, YELLOW_COLOR, EMPTY
from resource.ColorType import RED_TYPE, GREEN_TYPE, BLUE_TYPE, YELLOW_TYPE, BUTTON_TYPE
from resource.ButtonType import CIRCLE, PLUS, DIAGONAL
from handler.CustomWidget import CustomWidget
from handler.CustomButton import CustomButton
from handler.GameInfoBox import GameInfoBox
from helper.GameScreenHelper import GameScreenHelper
from helper.GeneticsHelper import GeneticsHelper

Builder.load_file("view/GameScreen.kv")


class GameScreen(Screen):
    map_x_size = 20
    map_y_size = 20
    number_of_buttons = 60

    def __init__(self, **kwargs):
        self.object_array = np.array([[None for x in range(self.map_x_size)] for y in range(self.map_y_size)])
        self.button_index_list = []
        self.game_grid = ObjectProperty(None)

        self.gameInfoBox = ObjectProperty(None)
        self.gameScreenHelper = GameScreenHelper(
            element_array=np.array(self.object_array),
            x=self.map_x_size, y=self.map_y_size)
        self.geneticsHelper = GeneticsHelper()

        self.bind_trigger = Clock.create_trigger(self.bind_data)
        super(GameScreen, self).__init__(**kwargs)

    def create_grid(self):
        temp_button_index_list = []
        main_color = random.choice([RED_TYPE, BLUE_TYPE, GREEN_TYPE, YELLOW_TYPE])
        self.geneticsHelper.default_color = main_color
        for row in range(self.game_grid.rows):
            for column in range(self.game_grid.cols):
                if (row, column) in self.button_index_list:
                    button = self.gameScreenHelper.create_button(row=row, column=column)
                    self.game_grid.add_widget(button)
                    self.object_array[button.row][button.column] = button
                    temp_button_index_list.append((row, column))
                else:
                    widget = self.gameScreenHelper.create_widget(row=row, column=column, color_type=main_color)
                    self.game_grid.add_widget(widget)
                    self.object_array[widget.row][widget.column] = widget

        self.button_index_list = temp_button_index_list

    def lever_action(self, *args):
        for position in self.gameScreenHelper.button_action(button_type=args[0].type, button_row=args[0].row,
                                                            button_column=args[0].column):
            self.update_widget_color(position=position)

    def update_widget_color(self, position):
        self.object_array[position[0]][position[1]].rgb = self.gameScreenHelper.get_color_by_position(position=position)

    def update_grid(self, grid):
        self.gameScreenHelper.element_array = grid
        for row in range(len(grid)):
            for column in range(len(grid[row])):
                if grid[row][column] != BUTTON_TYPE:
                    self.object_array[row][column].rgb = self.gameScreenHelper.get_color_by_type(grid[row][column])

    def randomize_grid(self):
        for button_index in self.button_index_list:
            for i in range(random.randint(0, 3)):
                self.object_array[button_index[0]][button_index[1]].button_action()

    def bind_data(self, *args):
        self.button_index_list = self.gameScreenHelper.create_button_map(
            x=self.map_x_size, y=self.map_y_size, number_of_buttons=self.number_of_buttons)

        self.create_grid()
        self.randomize_grid()

        self.geneticsHelper.button_index_list = np.array(self.button_index_list)
        self.geneticsHelper.button_type_list = np.array(self.gameScreenHelper.button_type_list)
        self.geneticsHelper.genome_size = len(self.geneticsHelper.button_type_list)
        self.geneticsHelper.original_grid = self.gameScreenHelper.element_array

        self.geneticsHelper.gameInfoBox = self.gameInfoBox
        self.geneticsHelper.gameScreenHelper = self.gameScreenHelper
        self.geneticsHelper.gameScreen = self

    def reset(self):
        if self.gameInfoBox.isSolving:
            self.gameInfoBox.solve()
        parent = self.children[0].children[1]
        for c in range(len(parent.children)):
            parent.remove_widget(parent.children[0])
        self.bind_trigger()

        if self.gameInfoBox.isSolving:
            self.gameInfoBox.solve()

        self.gameInfoBox.mutation_chance.value = 0
        self.gameInfoBox.mutation_intensity.value = 0
        self.gameInfoBox.generation_size.value = 5
        self.gameInfoBox.parents_in_family.value = 2
        self.gameInfoBox.number_of_families.value = 1
        self.gameInfoBox.children_in_family.value = 1

    def solve(self, isSolving):
        if not self.geneticsHelper.isRunning:
            self.geneticsHelper.start()

        if isSolving:
            self.geneticsHelper.resume()
        else:
            self.geneticsHelper.stop()

    def on_enter(self, *args):
        self.bind_trigger()
