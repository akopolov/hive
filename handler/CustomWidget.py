from kivy.lang import Builder
from kivy.properties import ListProperty
from kivy.uix.widget import Widget

from resource.ColorPackage import EMPTY

Builder.load_file("view/CustomWidget.kv")


class CustomWidget(Widget):
    rgb = ListProperty(EMPTY)

    def __init__(self, **kwargs):
        super(CustomWidget, self).__init__(**kwargs)
        self.bind(rgb=self.redraw)
        self.row = None
        self.column = None
        self.color_type = None
        self.isLever = False

    def redraw(self, *args):
        pass
