from kivy.lang import Builder
from kivy.clock import Clock
from kivy.uix.boxlayout import BoxLayout
from kivy.properties import ObjectProperty, NumericProperty
from resource.ColorPackage import RED_COLOR, BLUE_COLOR, GREEN_COLOR, YELLOW_COLOR, EMPTY
from resource.ColorType import RED_TYPE, GREEN_TYPE, BLUE_TYPE, YELLOW_TYPE, BUTTON_TYPE

Builder.load_file("view/GameInfoBox.kv")


class GameInfoBox(BoxLayout):

    def __init__(self, **kwargs):
        self.generation_size = ObjectProperty(None)
        self.parents_in_family = ObjectProperty(None)
        self.children_in_family = ObjectProperty(None)
        self.number_of_families = ObjectProperty(None)

        self.mutation_chance = ObjectProperty(None)
        self.mutation_intensity = ObjectProperty(None)

        self.black = ObjectProperty(None)
        self.red = ObjectProperty(None)
        self.green = ObjectProperty(None)
        self.blue = ObjectProperty(None)
        self.yellow = ObjectProperty(None)

        self.solve_button = ObjectProperty(None)
        self.isSolving = False
        super(GameInfoBox, self).__init__(**kwargs)

    def check_box_action(self, radio_button):
        pass

    def get_active_color(self):
        if self.black.active:
            return BUTTON_TYPE

        if self.red.active:
            return RED_TYPE

        if self.green.active:
            return GREEN_TYPE

        if self.blue.active:
            return BLUE_TYPE

        if self.yellow.active:
            return YELLOW_TYPE

    def get_mutation_chance(self):
        return float("{0:.2f}".format(self.mutation_chance.value))

    def get_mutation_intensity(self):
        return float("{0:.2f}".format(self.mutation_intensity.value))

    def get_generation_size(self):
        return int(self.generation_size.value)

    def get_parents_in_family(self):
        return int(self.parents_in_family.value)

    def get_children_in_family(self):
        children_in_family = int(self.children_in_family.value)
        difference = self.get_generation_size() - (children_in_family * self.get_number_of_families())
        if difference <= 0:
            return children_in_family
        else:
            return children_in_family + difference

    def get_number_of_families(self):
        return int(self.number_of_families.value)

    def reset(self, *args):
        self.parent.parent.reset()

    def solve(self):
        self.isSolving = not self.isSolving
        if self.isSolving:
            self.solve_button.children[0].children[0].text = 'Stop'
            self.solve_button.icon = 'stop'
        else:
            self.solve_button.children[0].children[0].text = 'Solve'
            self.solve_button.icon = 'play'
        self.parent.parent.solve(self.isSolving)
