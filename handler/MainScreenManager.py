from kivy.lang import Builder
from kivy.properties import ObjectProperty
from kivy.uix.screenmanager import ScreenManager

# NB !!!!!!!! DO NOT REMOVE !!!!!!!!!!
from handler.GameScreen import GameScreen

Builder.load_file("view/MainScreenManager.kv")


class MainScreenManager(ScreenManager):
    token = None

    def __init__(self, **kwargs):
        super(MainScreenManager, self).__init__(**kwargs)
        self.gameScreen = ObjectProperty(None)
