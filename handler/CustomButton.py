from kivy.lang import Builder
from kivy.properties import ListProperty
from kivy.uix.button import Button

from resource.ColorPackage import EMPTY, GRAY_COLOR

Builder.load_file("view/CustomButton.kv")


class CustomButton(Button):
    rgb = ListProperty(GRAY_COLOR)

    def __init__(self, **kwargs):
        super(CustomButton, self).__init__(**kwargs)
        self.bind(rgb=self.redraw)
        self.row = None
        self.column = None
        self.type = None
        self.isLever = True

    def redraw(self, *args):
        pass

    def button_action(self, *args):
        self.parent.parent.parent.lever_action(self)