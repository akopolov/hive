from kivy.app import App
from kivymd.theming import ThemeManager
from kivy.config import Config
from kivy.core.window import Window
from handler.MainScreenManager import MainScreenManager


class HiveMind(App):
    connectionManager = None
    theme_cls = ThemeManager()
    theme_cls.primary_palette = "Blue"
    Config.write()

    def build(self):
        return MainScreenManager()


if __name__ == '__main__':
    HiveMind().run()
